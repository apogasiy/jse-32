package com.tsc.apogasiy.tm.component;

import com.tsc.apogasiy.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService;

    @NotNull
    final Collection<String> commands = new ArrayList<>();

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }

    public void start() {
        for (@NotNull final AbstractCommand command : bootstrap.getCommandService().getCommands()) {
            if (command.hasArgs()) {
                commands.add(command.getCommand());
                System.out.println("FILE SCANNER INIT: " + command.getCommand());
            }
        }
        executorService.scheduleWithFixedDelay(this, 0, bootstrap.getPropertyService().getFileScannerInterval(), TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    @Override
    public void run() {
        @NotNull final File dir = new File(bootstrap.getPropertyService().getFileScannerDir());
        if (dir.exists()) {
            @NotNull final File[] files = dir.listFiles();
            Arrays.sort(files, Comparator.comparing(File::lastModified));
            for (final File file : files) {
                if (file.isFile()) {
                    @NotNull final String fileName = file.getName();
                    if (commands.contains(fileName)) {
                        bootstrap.runCommand(fileName, false);
                        file.delete();
                    }
                }
            }
        }
    }

}
