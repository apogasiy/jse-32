package com.tsc.apogasiy.tm.command;

import com.tsc.apogasiy.tm.api.service.IServiceLocator;
import com.tsc.apogasiy.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public Role[] roles() {
        return null;
    }

    @NotNull
    public abstract String getCommand();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    public abstract void execute();

    @Override
    @NotNull
    public String toString() {
        String result = "";
        @NotNull final String name = getCommand();
        @Nullable final String arg = getArgument();
        @NotNull final String description = getDescription();

        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        result = name + result + " - " + description;
        return result;
    }

    public boolean hasArgs() {
        if (getArgument() == null)
            return false;
        else
            return true;
    }

}
