package com.tsc.apogasiy.tm.command.data;

import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import com.tsc.apogasiy.tm.dto.Domain;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getCommand() {
        return "data-load-jaxb-json";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from json by JAXB";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        setDomain((Domain) unmarshaller.unmarshal(new File(serviceLocator.getPropertyService().getDTOJsonFileName())));
    }

}
