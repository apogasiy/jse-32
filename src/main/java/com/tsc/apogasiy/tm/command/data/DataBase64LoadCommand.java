package com.tsc.apogasiy.tm.command.data;

import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import com.tsc.apogasiy.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getCommand() {
        return "data-load-base64";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load state from base64";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final String data = new String(Files.readAllBytes(Paths.get(serviceLocator.getPropertyService().getDTOBase64FileName())));
        @NotNull final byte[] decodedData = Base64.getDecoder().decode(data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        setDomain((Domain) objectInputStream.readObject());
        objectInputStream.close();
        byteArrayInputStream.close();
    }

}
