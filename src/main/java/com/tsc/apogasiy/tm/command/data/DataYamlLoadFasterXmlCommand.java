package com.tsc.apogasiy.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import com.tsc.apogasiy.tm.dto.Domain;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getCommand() {
        return "data-load-fasterxml-yaml";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from yaml by FasterXML";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final String yaml = new String(Files.readAllBytes(Paths.get(serviceLocator.getPropertyService().getDTOYamlFileName())));
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        setDomain(objectMapper.readValue(yaml, Domain.class));
    }

}
