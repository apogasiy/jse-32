package com.tsc.apogasiy.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.tsc.apogasiy.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;

public class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getCommand() {
        return "data-save-fasterxml-json";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to json by FasterXML";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JaxbAnnotationModule());    // Поддержка аннотаций JAXB
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(getDomain());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(serviceLocator.getPropertyService().getDTOJsonFileName());
        fileOutputStream.write(json.getBytes());
        fileOutputStream.close();
    }

}