package com.tsc.apogasiy.tm.exception.empty;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Error! Description is empty!");
    }

}
